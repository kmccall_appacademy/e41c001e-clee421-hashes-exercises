# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  new_hash = {}
  str.split.each {|w| new_hash[w] = w.length}
  new_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k, v| v}.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each {|k, v| older[k] = v}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)
  word.each_char {|l| hash[l] += 1}
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = {}
  arr.each {|l| hash[l] = true}
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = {odd: 0, even: 0}
  numbers.each {|n| n.odd? ? hash[:odd] += 1 : hash[:even] += 1}
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  #not sure if caps matter
  hash = Hash.new(0)
  string.each_char {|v| hash[v] += 1 if "aeiou".include?(v)}
  hash.select {|k,v| v == hash.values.max}.keys.sort.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  res = []
  fall_win_hash = students.select {|k,v| v > 6}
  fall_win_hash.keys.combination(2){|a| res << a}
  res
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  #number_of_species**2 * smallest_population_size: / largest_population_size
  #number_of_species: specimen_hash.length
  #smallest_population_size: specimen_hash.values.min
  #largest_population_size: specimen_hash.values.max
  specimen_hash = Hash.new(0)
  specimens.each {|s| specimen_hash[s] += 1}
  specimen_hash.length**2 * specimen_hash.values.min / specimen_hash.values.max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  punc = ",.;:'?!'"
  norm_hash = Hash.new(0)
  vand_hash = Hash.new(0)

  normal_sign.downcase.delete(punc).each_char {|l| norm_hash[l] += 1}
  vandalized_sign.downcase.delete(punc).each_char {|l| vand_hash[l] += 1}

  vand_hash.each {|k, v| return false unless norm_hash[k] >= v}
  true
end

# def character_count(str)
# end
